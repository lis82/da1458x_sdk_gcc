#******************************************************************************
#
# Copyright (c) 2018 Siarhei Volkau
# SPDX-License-Identifier: MIT
#
#******************************************************************************

cmake_minimum_required(VERSION 3.3 FATAL_ERROR)

# this variable accumulates SDK C source files
set(DA580_SDK_SOURCES ${DA580_SDK_SOURCES} CACHE INTERNAL "")
set(DA580_LINK_LIBRARIES ${DA580_LINK_LIBRARIES} CACHE INTERNAL "")

#
# check supported SoC setted up properly
#
set(SUPPORTED_TARGETS "DA14580" "DA14581" "DA14583")

if (NOT (DA580_TARGET_SOC IN_LIST SUPPORTED_TARGETS))
	message(FATAL_ERROR
		"Please set valid DA580_TARGET_SOC. Valid targets are: ${SUPPORTED_TARGETS}."
	)
endif()

add_definitions("-D__weak=__attribute__((weak))")
add_definitions("-D'__nop()=__asm(\"nop\")'")
if (DA580_TARGET_SOC STREQUAL "DA14581")
	add_definitions(-D__DA14581__)
endif()

set_property(CACHE DA580_TARGET_SOC PROPERTY STRINGS ${SUPPORTED_TARGETS})

#
# Find SDK root
#
if (NOT DEFINED DA580_SDK_ROOT)
  set(DA580_SDK_ROOT ${CMAKE_CURRENT_LIST_DIR} CACHE INTERNAL "")
endif ()
# absolutize SDK root path
get_filename_component(DA580_SDK_ROOT "${DA580_SDK_ROOT}" ABSOLUTE)


#
# set compiler flags depending on target
#
set(CPU_FLAGS "-mthumb -mcpu=cortex-m0 -mfloat-abi=soft")

#
# search corresponding standard libraries and runtime
#
option(USE_NANO_STDC "Use nano standard library (reduces code size)" ON)
option(ENABLE_FLOAT_PRINTF "Enable float printf with nano standard library" OFF)
option(ENABLE_FLOAT_SCANF "Enable float scanf with nano standard library" OFF)

if (USE_NANO_STDC)
	set(NEWLIB_OPTIONS "${NEWLIB_OPTIONS} --specs=nano.specs")
	if (ENABLE_FLOAT_PRINTF)
		set(NEWLIB_OPTIONS "${NEWLIB_OPTIONS} -u _printf_float")
	endif()
	if (ENABLE_FLOAT_SCANF)
		set(NEWLIB_OPTIONS "${NEWLIB_OPTIONS} -u _scanf_float")
	endif()

	set(DA580_LINK_LIBRARIES ${DA580_LINK_LIBRARIES} m gcc c_nano nosys)
else()
	set(DA580_LINK_LIBRARIES ${DA580_LINK_LIBRARIES} m gcc c nosys)
endif()

#
# search corresponding standard libraries
#
execute_process(
	COMMAND ${CMAKE_C_COMPILER} "-mthumb" "-mcpu=cortex-m0" "-mfloat-abi=soft"
	"-print-file-name=libgcc.a" OUTPUT_VARIABLE LIBGCC_PATH
)
string(STRIP "${LIBGCC_PATH}" LIBGCC_PATH)
get_filename_component(LIBGCC_PATH "${LIBGCC_PATH}" DIRECTORY)
get_filename_component(LIBGCC_PATH "${LIBGCC_PATH}" ABSOLUTE)

execute_process(
	COMMAND ${CMAKE_C_COMPILER} "-mthumb" "-mcpu=cortex-m0" "-mfloat-abi=soft"
	"-print-file-name=libc.a" OUTPUT_VARIABLE LIBC_PATH
)
string(STRIP "${LIBC_PATH}" LIBC_PATH)
get_filename_component(LIBC_PATH "${LIBC_PATH}" DIRECTORY)
get_filename_component(LIBC_PATH "${LIBC_PATH}" ABSOLUTE)

set(LINKER_SEARCH_PATHS "${LINKER_SEARCH_PATHS} -L${LIBGCC_PATH} -L${LIBC_PATH}")
message(STATUS ${LINKER_SEARCH_PATHS})

set(CMAKE_C_LINK_EXECUTABLE "${CMAKE_C_LINKER} --no-wchar-size-warning ${DA580_WRAPPED_FUNCS} -T${DA580_LINKER_SCRIPT} -Map=${CMAKE_PROJECT_NAME}.map ${LINKER_SEARCH_PATHS} --gc-sections -o <TARGET> <OBJECTS> <LINK_LIBRARIES>")
set(CMAKE_CXX_LINK_EXECUTABLE "${CMAKE_CXX_LINKER} --no-wchar-size-warning -T${DA580_LINKER_SCRIPT} ${DA580_WRAPPED_FUNCS} -Map=${CMAKE_PROJECT_NAME}.map ${LINKER_SEARCH_PATHS} --gc-sections -o <TARGET> <OBJECTS> <LINK_LIBRARIES>")

#
# define volatile compiler flags
#
option(USE_LTO_DEBUG "Use link time optimization in debug build" OFF)
option(USE_LTO_RELEASE "Use link time optimization in release build" ON)
if (USE_LTO_DEBUG)
	set(LTO_FLAGS_DEBUG "-ftree-switch-conversion")
endif()
if (USE_LTO_RELEASE)
	set(LTO_FLAGS_RELEASE "-ftree-switch-conversion")
endif()

# FIXME those flags aren't cached properly
set(CMAKE_C_FLAGS "${CPU_FLAGS} -std=c99 -ffunction-sections -fdata-sections -fshort-enums -Wno-attributes -g3 ${NEWLIB_OPTIONS} ${DA580_PREINCLUDES}")
set(CMAKE_C_FLAGS_DEBUG "-Os ${LTO_FLAGS_DEBUG}")
set(CMAKE_C_FLAGS_RELEASE "-O2 ${LTO_FLAGS_RELEASE}")
set(CMAKE_C_FLAGS_RELWITHDEBINFO "-O2 -g3 ${LTO_FLAGS_RELEASE}")
set(CMAKE_C_FLAGS_MINSIZEREL "-Os ${LTO_FLAGS_RELEASE}")

set(CMAKE_CXX_FLAGS "${CPU_FLAGS} -std=c++11 -ffunction-sections -fdata-sections -fshort-enums -g3 -Wall -Wextra ${NEWLIB_OPTIONS}")
set(CMAKE_CXX_FLAGS_DEBUG ${CMAKE_C_FLAGS_DEBUG})
set(CMAKE_CXX_FLAGS_RELEASE ${CMAKE_C_FLAGS_RELEASE})
set(CMAKE_CXX_FLAGS_RELWITHDEBINFO ${CMAKE_C_FLAGS_RELWITHDEBINFO})
set(CMAKE_CXX_FLAGS_MINSIZEREL ${CMAKE_C_FLAGS_MINSIZEREL})

set(CMAKE_ASM_FLAGS "${CPU_FLAGS} -ffunction-sections -fdata-sections -fshort-enums -g3 ${NEWLIB_OPTIONS}")
set(CMAKE_ASM_FLAGS_DEBUG ${CMAKE_C_FLAGS_DEBUG})
set(CMAKE_ASM_FLAGS_RELEASE ${CMAKE_C_FLAGS_RELEASE})
set(CMAKE_ASM_FLAGS_RELWITHDEBINFO ${CMAKE_C_FLAGS_RELWITHDEBINFO})
set(CMAKE_ASM_FLAGS_MINSIZEREL ${CMAKE_C_FLAGS_MINSIZEREL})

#
# print some debug info
#
message(STATUS "DA1458x SDK path: ${DA580_SDK_ROOT}")
message(STATUS "Toolchain path: ${TOOLCHAIN_PATH}")

include_directories(${DA580_SDK_ROOT}/platform/include)
include_directories(${DA580_SDK_ROOT}/platform/arch)
include_directories(${DA580_SDK_ROOT}/platform/arch/compiler/rvds)
include_directories(${DA580_SDK_ROOT}/platform/arch/ll/rvds)
include_directories(${DA580_SDK_ROOT}/platform/driver/reg)
# include sources depending on modules to be used
if (adc IN_LIST DA580_USE_MODULES)
	include_directories(${DA580_SDK_ROOT}/platform/driver/adc)
	list(APPEND DA580_SDK_SOURCES
		${DA580_SDK_ROOT}/platform/driver/adc/adc_gcc.c
	)
endif()
if (battery IN_LIST DA580_USE_MODULES)
	include_directories(${DA580_SDK_ROOT}/platform/driver/battery)
	list(APPEND DA580_SDK_SOURCES
		${DA580_SDK_ROOT}/platform/driver/battery/battery_gcc.c
	)
endif()
if (gpio IN_LIST DA580_USE_MODULES)
	include_directories(${DA580_SDK_ROOT}/platform/driver/gpio)
	list(APPEND DA580_SDK_SOURCES
		${DA580_SDK_ROOT}/platform/driver/gpio/gpio.c
	)
endif()
if (uart IN_LIST DA580_USE_MODULES)
	include_directories(${DA580_SDK_ROOT}/platform/driver/uart)
	list(APPEND DA580_SDK_SOURCES
		${DA580_SDK_ROOT}/platform/driver/uart/uart_gcc.c
	)
endif()

if (uart2 IN_LIST DA580_USE_MODULES)
	include_directories(${DA580_SDK_ROOT}/platform/driver/uart)
	list(APPEND DA580_SDK_SOURCES
		${DA580_SDK_ROOT}/platform/driver/uart/uart2_gcc.c
	)
endif()

if (i2c-eeprom IN_LIST DA580_USE_MODULES)
	include_directories(${DA580_SDK_ROOT}/platform/driver/i2c_eeprom)
	list(APPEND DA580_SDK_SOURCES
		${DA580_SDK_ROOT}/platform/driver/i2c_eeprom/i2c_eeprom_gcc.c
	)
endif()

if (pwm IN_LIST DA580_USE_MODULES)
	include_directories(${DA580_SDK_ROOT}/platform/driver/pwm)
	list(APPEND DA580_SDK_SOURCES
		${DA580_SDK_ROOT}/platform/driver/pwm/pwm_gcc.c
	)
endif()

if (qdec IN_LIST DA580_USE_MODULES)
	include_directories(${DA580_SDK_ROOT}/platform/driver/wkupct_quadec)
	list(APPEND DA580_SDK_SOURCES
		${DA580_SDK_ROOT}/platform/driver/wkupct_quadec/wkupct_quadec_gcc.c
	)
endif()

if (syscntl IN_LIST DA580_USE_MODULES)
	include_directories(${DA580_SDK_ROOT}/platform/driver/syscntl)
	list(APPEND DA580_SDK_SOURCES
		${DA580_SDK_ROOT}/platform/driver/syscntl/syscntl.c
	)
endif()

if (systick IN_LIST DA580_USE_MODULES)
	include_directories(${DA580_SDK_ROOT}/platform/driver/systick)
	list(APPEND DA580_SDK_SOURCES
		${DA580_SDK_ROOT}/platform/driver/systick/systick_gcc.c
	)
endif()

if (spi IN_LIST DA580_USE_MODULES)
	include_directories(${DA580_SDK_ROOT}/platform/driver/spi)
	list(APPEND DA580_SDK_SOURCES
		${DA580_SDK_ROOT}/platform/driver/spi/spi.c
	)
endif()

if (spi-flash IN_LIST DA580_USE_MODULES)
	include_directories(${DA580_SDK_ROOT}/platform/driver/spi_flash)
	list(APPEND DA580_SDK_SOURCES
		${DA580_SDK_ROOT}/platform/driver/spi_flash/spi_flash.c
	)
endif()

if (trng IN_LIST DA580_USE_MODULES)
	include_directories(${DA580_SDK_ROOT}/platform/driver/trng)
	list(APPEND DA580_SDK_SOURCES
		${DA580_SDK_ROOT}/platform/driver/trng/trng.c
	)
endif()

#
# build intel hex file
#
add_custom_command(OUTPUT ${CMAKE_PROJECT_NAME}.hexfile
	COMMAND ${CMAKE_OBJCOPY} -O ihex ${CMAKE_PROJECT_NAME}.elf ${CMAKE_PROJECT_NAME}.hex
	COMMENT "Making hex file ${CMAKE_PROJECT_NAME}.hex"
)

add_custom_target(${CMAKE_PROJECT_NAME}.hex ALL
	DEPENDS ${CMAKE_PROJECT_NAME}.hexfile ${CMAKE_PROJECT_NAME}
)

#
# build raw binary file
#
add_custom_command(OUTPUT ${CMAKE_PROJECT_NAME}.binfile
	COMMAND ${CMAKE_OBJCOPY} -O binary ${CMAKE_PROJECT_NAME}.elf ${CMAKE_PROJECT_NAME}.bin
	COMMENT "Making bin file ${CMAKE_PROJECT_NAME}.bin"
)

add_custom_target(${CMAKE_PROJECT_NAME}.bin ALL
	DEPENDS ${CMAKE_PROJECT_NAME}.binfile ${CMAKE_PROJECT_NAME}
)
