/**************************************************************************//**
 * @file uart2_gcc.c
 *
 * Copyright (c) 2018 Siarhei Volkau
 * SPDX-License-Identifier: MIT
 *
 ******************************************************************************/

/* fix build with gcc */
/* inline should be with static */
#include "arch.h"
#include "compiler.h"
#undef __INLINE
#define __INLINE static inline

#include "rwip_config.h"
#include "reg_blecore.h"        // ble core registers
#include "reg_ble_em_cs.h"      // control structure definitions
#include "reg_access.h"

/* return declaration back */
#undef __INLINE
#define __INLINE inline

#pragma GCC diagnostic ignored "-Wattributes"

#include "rf_580.c"
