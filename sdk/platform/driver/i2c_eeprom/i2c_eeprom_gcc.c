/**************************************************************************//**
 * @file i2c_eeprom_gcc.c
 *
 * Copyright (c) 2018 Siarhei Volkau
 * SPDX-License-Identifier: MIT
 *
 ******************************************************************************/

#pragma GCC diagnostic ignored "-Wsign-compare"

#include "i2c_eeprom.c"
