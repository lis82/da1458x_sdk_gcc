/**************************************************************************//**
 * @file adc_gcc.c
 *
 * Copyright (c) 2018 Siarhei Volkau
 * SPDX-License-Identifier: MIT
 *
 ******************************************************************************/

/* introduce missing intrinsic */
#define __nop() __asm("nop")

#include "adc.c"
