/**************************************************************************//**
 * @file wkupct_quadec_gcc.c
 *
 * Copyright (c) 2018 Siarhei Volkau
 * SPDX-License-Identifier: MIT
 *
 ******************************************************************************/

#pragma GCC diagnostic ignored "-Wattributes"

#include "wkupct_quadec.c"
