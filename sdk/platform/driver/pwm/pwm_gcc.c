/**************************************************************************//**
 * @file pwm_gcc.c
 *
 * Copyright (c) 2018 Siarhei Volkau
 * SPDX-License-Identifier: MIT
 *
 ******************************************************************************/

/* see linker script PROVIDE() for understanding
 * how to ARMCC's `at(_address_)` attribute simulated
 */
#pragma GCC diagnostic ignored "-Wattributes"

#include "pwm.c"
