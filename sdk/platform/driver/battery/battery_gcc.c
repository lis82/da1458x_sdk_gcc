/**************************************************************************//**
 * @file battery_gcc.c
 *
 * Copyright (c) 2018 Siarhei Volkau
 * SPDX-License-Identifier: MIT
 *
 ******************************************************************************/

#pragma GCC diagnostic ignored "-Wattributes"

#include "battery.c"
