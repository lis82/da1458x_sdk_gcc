/**************************************************************************//**
 * @file systick_gcc.c
 *
 * Copyright (c) 2018 Siarhei Volkau
 * SPDX-License-Identifier: MIT
 *
 ******************************************************************************/

 /* fix code for GCC */
#define $Sub$$SysTick_Handler SysTick_Handler

#include "systick.c"
