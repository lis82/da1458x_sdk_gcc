/**************************************************************************//**
 * @file uart2_gcc.c
 *
 * Copyright (c) 2018 Siarhei Volkau
 * SPDX-License-Identifier: MIT
 *
 ******************************************************************************/

/* fix build with gcc */
/* inline should be with static */
#include "arch.h"
#include "compiler.h"
#undef __INLINE
#define __INLINE static inline

#include "reg_uart.h"

/* return declaration back */
#undef __INLINE
#define __INLINE inline

#pragma GCC diagnostic ignored "-Wattributes"

#include "uart2.c"
