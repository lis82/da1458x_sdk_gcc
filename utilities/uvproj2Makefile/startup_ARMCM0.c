/**************************************************************************//**
 * @file startup_ARMCM0.c
 *
 * Copyright (c) 2018 Siarhei Volkau
 * SPDX-License-Identifier: MIT
 *
 ******************************************************************************/

#include <stdint.h>
#include <string.h>

extern uint32_t __bss_start__;
extern uint32_t __bss_end__;
extern uint32_t __etext;
extern uint32_t __data_start__;
extern uint32_t __data_end__;

extern void SystemInit(void);
extern int main(void);
extern void NMI_HandlerC(unsigned long *hardfault_args);
extern void HardFault_HandlerC(unsigned long *hardfault_args);
extern void SVC_Handler_c(unsigned int * svc_args);

#ifndef STACK_SIZE
#define STACK_SIZE (0x400)
#endif
__attribute__ ((section(".stack")))
static uint8_t Stack_Mem[STACK_SIZE] __attribute__((aligned(8)));

// Reset Handler
__attribute__((naked))
__attribute__ ((weak))
void Reset_Handler(void)
{
	/* copy .data from LMA to VMA */
	intptr_t len = &__data_end__ - &__data_start__;
	if (&__etext != &__data_start__ && len > 0) {
		memmove(&__data_start__, &__etext, len);
	}

#if defined (__STARTUP_CLEAR_BSS)
	/* clear bss */
	uint32_t *ptr = &__bss_start__;
	while(ptr < &__bss_end__)
		*(ptr++) = 0;
#endif

#ifndef __NO_SYSTEM_INIT
	/* init & clear retention ram and some other stuff */
	SystemInit();
#endif

#ifndef __START
#define __START _start
#endif
	__START();

	/* guard */
	while(1);
}


// Dummy Exception Handlers (infinite loops which can be modified)

__attribute__((naked))
__attribute__ ((weak))
void NMI_Handler(void)
{
	register uint32_t lr;
	register void (*c_handler)(unsigned long *) = NMI_HandlerC;

	__asm("mov %0, lr" : "=r" (lr));
	if ((lr & 4) == 0) {
		__asm("mrs r0, msp");
	} else {
		__asm("mrs r0, psp");
	}
	// NMI_HandlerC(r0 = ?sp);
	__asm("bx %0" : : "r" (c_handler));
}

__attribute__((naked))
__attribute__ ((weak))
void HardFault_Handler(void)
{
	register uint32_t lr;
	register void (*c_handler)(unsigned long *) = HardFault_HandlerC;

	__asm("mov %0, lr" : "=r" (lr));
	if ((lr & 4) == 0) {
		__asm("mrs r0, msp");
	} else {
		__asm("mrs r0, psp");
	}
	// HardFault_HandlerC(r0 = ?sp);
	__asm("bx %0" : : "r" (c_handler));
}

__attribute__((naked))
__attribute__ ((weak))
void SVC_Handler(void)
{
	register uint32_t lr;
	register void (*c_handler)(unsigned int *) = SVC_Handler_c;

	__asm("mov %0, lr" : "=r" (lr));
	if ((lr & 4) == 0) {
		__asm("mrs r0, msp");
	} else {
		__asm("mrs r0, psp");
	}
	// SVC_Handler_c(r0 = ?sp);
	__asm("bx %0" : : "r" (c_handler));
}

__attribute__((naked))
__attribute__ ((weak))
void PendSV_Handler(void)
{
	while(1);
}

__attribute__((naked))
__attribute__ ((weak))
void SysTick_Handler(void)
{
	while(1);
}

__attribute__((naked))
void Default_Handler(void)
{
	while(1);
}

void BLE_WAKEUP_LP_Handler(void)  __attribute__((weak, alias("Default_Handler")));
//void BLE_FINETGTIM_Handler(void)  __attribute__((weak, alias("Default_Handler")));
extern void BLE_FINETGTIM_Handler(void);
//void BLE_GROSSTGTIM_Handler(void) __attribute__((weak, alias("Default_Handler")));
extern void BLE_GROSSTGTIM_Handler(void);
//void BLE_CSCNT_Handler(void)      __attribute__((weak, alias("Default_Handler")));
extern void BLE_CSCNT_Handler(void);
void BLE_SLP_Handler(void)        __attribute__((weak, alias("Default_Handler")));
//void BLE_ERROR_Handler(void)      __attribute__((weak, alias("Default_Handler")));
extern void BLE_ERROR_Handler(void);
//void BLE_RX_Handler(void)         __attribute__((weak, alias("Default_Handler")));
extern void BLE_RX_Handler(void);
//void BLE_EVENT_Handler(void)      __attribute__((weak, alias("Default_Handler")));
extern void BLE_EVENT_Handler(void);
void SWTIM_Handler(void)          __attribute__((weak, alias("Default_Handler")));
void WKUP_QUADEC_Handler(void)    __attribute__((weak, alias("Default_Handler")));
//void BLE_RF_DIAG_Handler(void)  __attribute__((weak, alias("Default_Handler")));
extern void BLE_RF_DIAG_Handler(void);
//void BLE_CRYPT_Handler(void)      __attribute__((weak, alias("Default_Handler")));
extern void BLE_CRYPT_Handler(void);
void UART_Handler(void)           __attribute__((weak, alias("Default_Handler")));
void UART2_Handler(void)          __attribute__((weak, alias("Default_Handler")));
void I2C_Handler(void)            __attribute__((weak, alias("Default_Handler")));
void SPI_Handler(void)            __attribute__((weak, alias("Default_Handler")));
void ADC_Handler(void)            __attribute__((weak, alias("Default_Handler")));
void KEYBRD_Handler(void)         __attribute__((weak, alias("Default_Handler")));
void RFCAL_Handler(void)          __attribute__((weak, alias("Default_Handler")));
void GPIO0_Handler(void)          __attribute__((weak, alias("Default_Handler")));
void GPIO1_Handler(void)          __attribute__((weak, alias("Default_Handler")));
void GPIO2_Handler(void)          __attribute__((weak, alias("Default_Handler")));
void GPIO3_Handler(void)          __attribute__((weak, alias("Default_Handler")));
void GPIO4_Handler(void)          __attribute__((weak, alias("Default_Handler")));

__attribute__ ((section(".isr_vector")))
void (* const __Vectors[])(void) =
{
	(void (*)(void))((void*)Stack_Mem + STACK_SIZE), // Top of Stack
	Reset_Handler,          // Reset Handler
	NMI_Handler,            // NMI Handler
	HardFault_Handler,      // Hard Fault Handler
	0,                      // Reserved
	0,                      // Reserved
	0,                      // Reserved
	0,                      // Reserved
	0,                      // Reserved
	0,                      // Reserved
	0,                      // Reserved
	SVC_Handler,            // SVCall Handler
	0,                      // Reserved
	0,                      // Reserved
	PendSV_Handler,         // PendSV Handler
	SysTick_Handler,        // SysTick Handler
	BLE_WAKEUP_LP_Handler,
	BLE_FINETGTIM_Handler,
	BLE_GROSSTGTIM_Handler,
	BLE_CSCNT_Handler,
	BLE_SLP_Handler,
	BLE_ERROR_Handler,
	BLE_RX_Handler,
	BLE_EVENT_Handler,
	SWTIM_Handler,
	WKUP_QUADEC_Handler,
	BLE_RF_DIAG_Handler,
	BLE_CRYPT_Handler,
	UART_Handler,
	UART2_Handler,
	I2C_Handler,
	SPI_Handler,
	ADC_Handler,
	KEYBRD_Handler,
	RFCAL_Handler,
	GPIO0_Handler,
	GPIO1_Handler,
	GPIO2_Handler,
	GPIO3_Handler,
	GPIO4_Handler,
};
