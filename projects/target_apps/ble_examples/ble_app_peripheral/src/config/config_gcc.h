/**************************************************************************//**
 * @file config_gcc.h
 *
 * Copyright (c) 2018 Siarhei Volkau
 * SPDX-License-Identifier: MIT
 *
 ******************************************************************************/

#ifndef CONFIG_GCC_H_
#define CONFIG_GCC_H_

/* fix build with gcc */
/* inline should be with static */
#include "compiler.h"
#undef __INLINE
#define __INLINE static inline

#endif
