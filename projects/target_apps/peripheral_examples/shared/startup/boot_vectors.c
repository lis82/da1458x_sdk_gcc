/**************************************************************************//**
 * @file boot_vectors.c
 *
 * Copyright (c) 2018 Siarhei Volkau
 * SPDX-License-Identifier: MIT
 *
 ******************************************************************************/

#include <stdint.h>

extern uint32_t __bss_start__;
extern uint32_t __bss_end__;

extern void SystemInit(void);
extern int main(void);

#ifndef STACK_SIZE
#define STACK_SIZE (0x400)
#endif
__attribute__ ((section(".stack")))
static uint8_t Stack_Mem[STACK_SIZE] __attribute__((aligned(8)));

// Reset Handler
__attribute__((naked))
__attribute__ ((weak))
void Reset_Handler(void)
{
	/* clear bss */
	uint32_t *ptr = &__bss_start__;
	while(ptr < &__bss_end__)
		*(ptr++) = 0;

	SystemInit();
	main();
	while(1);
}


// Dummy Exception Handlers (infinite loops which can be modified)

__attribute__((naked))
__attribute__ ((weak))
void NMI_Handler(void)
{
	while(1);
}

__attribute__((naked))
__attribute__ ((weak))
void HardFault_Handler(void)
{
	while(1);
}

__attribute__((naked))
__attribute__ ((weak))
void SVC_Handler(void)
{
	while(1);
}

__attribute__((naked))
__attribute__ ((weak))
void PendSV_Handler(void)
{
	while(1);
}

__attribute__((naked))
__attribute__ ((weak))
void SysTick_Handler(void)
{
	while(1);
}

__attribute__((naked))
void Default_Handler(void)
{
	while(1);
}

void BLE_WAKEUP_LP_Handler(void)  __attribute__((weak, alias("Default_Handler")));
void BLE_FINETGTIM_Handler(void)  __attribute__((weak, alias("Default_Handler")));
void BLE_GROSSTGTIM_Handler(void) __attribute__((weak, alias("Default_Handler")));
void BLE_CSCNT_Handler(void)      __attribute__((weak, alias("Default_Handler")));
void BLE_SLP_Handler(void)        __attribute__((weak, alias("Default_Handler")));
void BLE_ERROR_Handler(void)      __attribute__((weak, alias("Default_Handler")));
void BLE_RX_Handler(void)         __attribute__((weak, alias("Default_Handler")));
void BLE_EVENT_Handler(void)      __attribute__((weak, alias("Default_Handler")));
void SWTIM_Handler(void)          __attribute__((weak, alias("Default_Handler")));
void WKUP_QUADEC_Handler(void)    __attribute__((weak, alias("Default_Handler")));
void BLE_RADIOCNTL_Handler(void)  __attribute__((weak, alias("Default_Handler")));
void BLE_CRYPT_Handler(void)      __attribute__((weak, alias("Default_Handler")));
void UART_Handler(void)           __attribute__((weak, alias("Default_Handler")));
void UART2_Handler(void)          __attribute__((weak, alias("Default_Handler")));
void I2C_Handler(void)            __attribute__((weak, alias("Default_Handler")));
void SPI_Handler(void)            __attribute__((weak, alias("Default_Handler")));
void ADC_Handler(void)            __attribute__((weak, alias("Default_Handler")));
void KEYBRD_Handler(void)         __attribute__((weak, alias("Default_Handler")));
void RFCAL_Handler(void)          __attribute__((weak, alias("Default_Handler")));
void GPIO0_Handler(void)          __attribute__((weak, alias("Default_Handler")));
void GPIO1_Handler(void)          __attribute__((weak, alias("Default_Handler")));
void GPIO2_Handler(void)          __attribute__((weak, alias("Default_Handler")));
void GPIO3_Handler(void)          __attribute__((weak, alias("Default_Handler")));
void GPIO4_Handler(void)          __attribute__((weak, alias("Default_Handler")));

__attribute__ ((section(".isr_vector")))
void (* const __Vectors[])(void) =
{
	(void (*)(void))((void*)Stack_Mem + STACK_SIZE), // Top of Stack
	Reset_Handler,          // Reset Handler
	NMI_Handler,            // NMI Handler
	HardFault_Handler,      // Hard Fault Handler
	0,                      // Reserved
	0,                      // Reserved
	0,                      // Reserved
	0,                      // Reserved
	0,                      // Reserved
	0,                      // Reserved
	0,                      // Reserved
	SVC_Handler,            // SVCall Handler
	0,                      // Reserved
	0,                      // Reserved
	PendSV_Handler,         // PendSV Handler
	SysTick_Handler,        // SysTick Handler
	BLE_WAKEUP_LP_Handler,
	BLE_FINETGTIM_Handler,
	BLE_GROSSTGTIM_Handler,
	BLE_CSCNT_Handler,
	BLE_SLP_Handler,
	BLE_ERROR_Handler,
	BLE_RX_Handler,
	BLE_EVENT_Handler,
	SWTIM_Handler,
	WKUP_QUADEC_Handler,
	BLE_RADIOCNTL_Handler,
	BLE_CRYPT_Handler,
	UART_Handler,
	UART2_Handler,
	I2C_Handler,
	SPI_Handler,
	ADC_Handler,
	KEYBRD_Handler,
	RFCAL_Handler,
	GPIO0_Handler,
	GPIO1_Handler,
	GPIO2_Handler,
	GPIO3_Handler,
	GPIO4_Handler,
};
