/**************************************************************************//**
 * @file ring_buffer_gcc.cc
 *
 * Copyright (c) 2018 Siarhei Volkau
 * SPDX-License-Identifier: MIT
 *
 ******************************************************************************/

/**
 * GCC C does not allow use const variables as array size initializers,
 * but C++ allows this - let's use C++ to build original SDK C sources
 */
extern "C" {
#include "ring_buffer.c"
}
