#******************************************************************************
#
# Copyright (c) 2018 Siarhei Volkau
# SPDX-License-Identifier: MIT
#
# Cmake build script for batt_lvl example on DA1458x
# type following commands in console for build project
#   mkdir build && cd build
#   cmake -DCMAKE_BUILD_TYPE=<Debug|Release> ..
#   cmake --build .
#
#******************************************************************************

cmake_minimum_required(VERSION 3.3)

# uncomment and edit to use its own compiler tool
#set(TOOLCHAIN_PATH "/usr/local/gcc-arm-none-eabi-5_4-2016q3/bin")

set(DA580_SDK_ROOT "${CMAKE_CURRENT_LIST_DIR}/../../../../../sdk")

set(DA580_TARGET_SOC "DA14580" CACHE STRING "Target SoC")

set(CMAKE_TOOLCHAIN_FILE "${DA580_SDK_ROOT}/common_project_files/cmake/compiler-gcc.cmake")

# name of the final binary files
project(batt_lvl C)
enable_language(ASM)

# include shared sources
set(PERIPHERAL_SHARED_PATH "${DA580_SDK_ROOT}/../projects/target_apps/peripheral_examples/shared")
include_directories(${PERIPHERAL_SHARED_PATH}/common_uart)

set(DA580_USE_MODULES
	uart2
	gpio
	battery
	adc
)

# use specific for peripheral_examples linker script
set(DA580_LINKER_SCRIPT "${PERIPHERAL_SHARED_PATH}/peripheral_examples.ld")
get_filename_component(DA580_LINKER_SCRIPT "${DA580_LINKER_SCRIPT}" ABSOLUTE)

# use SDK defaults boilerplate
include("${DA580_SDK_ROOT}/common_project_files/cmake/sdk-gcc.cmake")

# project sources and includes
include_directories(${CMAKE_CURRENT_LIST_DIR}/include)

add_executable(${CMAKE_PROJECT_NAME}
	# common peripheral sources
	${PERIPHERAL_SHARED_PATH}/startup/boot_vectors.c
	${PERIPHERAL_SHARED_PATH}/startup/system_ARMCM0.c
	${PERIPHERAL_SHARED_PATH}/common_uart/common_uart.c

	src/main.c
	src/user_periph_setup.c

# sources resolved automatically depending
# on selected project options
	${DA580_SDK_SOURCES}
)

set_target_properties(${CMAKE_PROJECT_NAME} PROPERTIES PREFIX "" SUFFIX ".elf")
target_link_libraries(${CMAKE_PROJECT_NAME} ${DA580_LINK_LIBRARIES})
